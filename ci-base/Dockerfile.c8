FROM gitlab-registry.cern.ch/linuxsupport/c8-base

# Install headless Java, and tools as needed by https://github.com/openshift/jenkins/blob/master/slave-base/Dockerfile.rhel8
# We create 2 folders for JENKINS_HOME for backwards compatibility with older Kubernetes plugin versions (/opt/app-root/jenkins)
# while newer versions use /home/jenkins.
# NB: the Kubernetes plugin for Jenkins will mount a volume in JENKINS_HOME as of 0.9.
RUN yum update -y && \
    yum install -y bc gettext git java-11-openjdk-headless java-1.8.0-openjdk-headless lsof rsync tar unzip which zip bzip2 && \
    yum clean all && \
    mkdir -p /opt/app-root/jenkins /home/jenkins && \
    chown -R 1001:0 /opt/app-root/jenkins /home/jenkins && \
    chmod -R g+w /opt/app-root/jenkins /home/jenkins && \
    chmod -R 775 /etc/alternatives && \
    chmod -R 775 /var/lib/alternatives && \
    chmod -R 775 /usr/lib/jvm && \
    chmod 775 /usr/bin && \
    chmod 775 /usr/share/man/man1 && \
    mkdir -p /var/lib/origin && \
    chmod 775 /var/lib/origin && \   
# Install other useful packages
    yum install -y \
    autoconf \
    automake \
    cern-get-sso-cookie \
    cmake \
    createrepo \
    gcc \
    git-lfs \
    http-parser \
    jq \
    nodejs \
    python3-ldap \
    python3-setuptools \
    python3-virtualenv \
    python3-pip \
    python3-pyyaml \
    npm \
    rpm-build \
    vim-enhanced \
    wget \
    subversion \
    doxygen \
    maven && \
    yum clean all 

# Copy upstream run-jnlp-client script and helpers
ADD openshift-jenkins-upstream/slave-base/contrib/bin/* /usr/local/bin/

# Copy our own entrypoint script with support for GitLab CI
COPY contrib/bin/* /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/run-ci-worker"]